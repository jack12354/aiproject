﻿How to run
-----------
1. Download unity https://unity3d.com/get-unity (1+ gigs)
2. Clone this repo git@bitbucket.org:jack12354/aiproject.git
3. Open existing project in Unity and navigate to where you cloned the repo.
4. Open project.
5. Make sure to open the scene Sandbox.unity
6. Press play button on top. Debug logs show up under the console window.
7. Press the "g" key to go.
8. Use the scene view to look at the heuristic color cost mapping and node path.

Documentation
-------------
We use a utility called Doxygen that generates html documentation. In the repository,
documentation can be found in the Docs\html\annotated.html file.
If using unity, you can also go to Window -> Documentation and Doxygen -> Browse Documentation.
The browse documentation html will show all of the classes we defined with comments.
