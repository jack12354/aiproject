﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Debug = UnityEngine.Debug;

/// <summary>
/// Our A* agent to find the goal checkpoint.
/// </summary>
public class AIBrain : Brain
{
    /// <summary>
    /// True if we want to show heuristic cost over the area of the plane. Red is high cost, green is low cost.
    /// </summary>
    private readonly bool showHeuristicDisplay = true;
    /// <summary>
    /// A demo variable to show an inconsistent heuristic.
    /// </summary>
    private readonly bool showInconsistency = false;
    /// <summary>
    /// Defines the type of heuristic the agent will use.
    /// </summary>
    private readonly HeuristicType heuristicType = HeuristicType.FullHeuristic;
    /// <summary>
    /// MoveInfo object collection that will be building a path to the goal.
    /// </summary>
    List<MoveInfo> mMoveInfos = new List<MoveInfo>();
    /// <summary>
    /// The step rate nodes are generated
    /// </summary>
    private readonly float kTimeStepPerMove = Time.fixedDeltaTime;
    /// <summary>
    /// Used for various time dependant operations.
    /// </summary>
    private readonly Stopwatch mDriveTimeStopwatch = new Stopwatch();
    /// <summary>
    /// Our car object we use to get a collider the shape of the car.
    /// </summary>
    private GameObject testCar;
    /// <summary>
    /// Our collider that is used to check if the car tries to find a path that is not wide enough for the car.
    /// </summary>
    private Collider testCarCollider;
    /// <summary>
    /// How many moves that occur.
    /// </summary>
    private int moveCounter = 0;
    /// <summary>
    /// How many expansions that occur.
    /// </summary>
    private static int expansionCounter = 0;
    /// <summary>
    /// The max allowed expansion of nodes allowed by the agent.
    /// </summary>
    public const int MAX_EXPANSIONS_ALLOWED = 1000000;
    /// <summary>
    /// Signal cost value for bad moves.
    /// </summary>
    public const float THIS_IS_REALLY_BAD = 99999f;
    /// <summary>
    /// A position collection added to on each new poisiton created. (Pre route)
    /// </summary>
    private List<Vector3> positionList = new List<Vector3>();
    /// <summary>
    /// A position collection of post route positions. Used to debug.
    /// </summary>
    private List<Vector3> actualPositionList = new List<Vector3>();
    /// <summary>
    /// Data reference of heuristic cost.
    /// </summary>
    private float[,] dataPoints;
    /// <summary>
    /// Flag to check if the heuristic has been calculated for gizmo.
    /// </summary>
    private bool calculated = false;
    /// <summary>
    /// Pixel density max for gizmo drawing.
    /// </summary>
    private float maxDP;
    /// <summary>
    /// Pixel density min for gizmo drawing.
    /// </summary>
    private float minDP;


    public override void Go()
    {
        mEnabled = false;
        BuildPathToGoal();
    }


    public override void Init(Checkpoint[] inCheckpoints, Collider[] inNoZones, float inGasAmount, float inTurnAmount)
    {
        mTurnVelocity = inTurnAmount;
        mForwardVelocity = inGasAmount;
        mCheckpoints = inCheckpoints;
        mNoZones = inNoZones;
    }


    public override MoveInfo GetMoveInfo()
    {
        // If we are not starting the move infos we have should be non moving
        if (!mEnabled) return new MoveInfo() { GasAmount = 0, TurnAmount = 0 };

        var index = (int)(mDriveTimeStopwatch.ElapsedMilliseconds / (kTimeStepPerMove * 10000.0f));
        moveCounter++;
        index = moveCounter;
        actualPositionList.Add(transform.position);
        if (index < mMoveInfos.Count)
        {
            return mMoveInfos[index];
        }
        // Otherwise signal to the vehicle to stop updating. See Vehicle.cs for more info.
        return new MoveInfo { GasAmount = 100 };
    }


    public override void BuildPathToGoal()
    {
        StartCoroutine(BuildPathAsync());
    }


    /// <summary>
    /// Allows us to see the path being built.
    /// </summary>
    /// <returns>An IEnumerator being consumed by the StartCoroutine method.</returns>
    private IEnumerator BuildPathAsync()
    {
        var openQueue = new List<Pair<float, State>>();

        // Get our car to test for path widths
        testCar = Instantiate(gameObject);
        Destroy(testCar.GetComponent<Vehicle>());
        testCarCollider = testCar.GetComponent<Collider>();
        Destroy(testCar.transform.GetChild(0).gameObject);

        openQueue.Add(new Pair<float, State>(GetHeuristicCost(GetInitialState()), GetInitialState()));
        int count = 0;

        // How many expansions each frame will do in real time
        const int expandsPerFrame = 10;
        do
        {
            if (count++ > expandsPerFrame)
            {
                count = 0;
                yield return null;
            }

            int minIndex = -1;
            float minValue = float.MaxValue;
            for (int index = 0; index < openQueue.Count; index++)
            {
                var pair = openQueue[index];
                if (pair.First < minValue)
                {
                    minValue = pair.First;
                    minIndex = index;
                }
            }

            var thisState = openQueue[minIndex].Second;

            if (IsGoal(thisState))
            {
                Debug.Log("Found a Goal, took " + expansionCounter + " expansions");
                mMoveInfos = thisState.PathToHere;
                mEnabled = true;
                mDriveTimeStopwatch.Start();
                foreach (var theRenderer in testCar.GetComponentsInChildren<MeshRenderer>())
                {
                    theRenderer.enabled = false;
                }
                yield break;
            }

            openQueue[minIndex].First = THIS_IS_REALLY_BAD;
            // pathCost = Sum of all state move costs
            float pathCost = thisState.PathToHere.Sum(moveInfo => GetCostOfAction(moveInfo));
            openQueue.AddRange(GetSuccessors(thisState).Select(state => new Pair<float, State>(GetHeuristicCost(state) + pathCost, state)));

            if (expansionCounter > MAX_EXPANSIONS_ALLOWED)
            {
                // Visual representation that we did not find a solution. Just going backwards.
                for (int i = 0; i < 500; i++)
                {
                    mMoveInfos.Add(new MoveInfo
                    {
                        GasAmount = -1.0f,
                        TurnAmount = 0
                    });
                }
                mEnabled = true;
                mDriveTimeStopwatch.Start();
                yield break;
            }

        } while (openQueue.Count > 0);
    }

    /// <summary>
    /// Tests a state to see if it has reached a goal.
    /// </summary>
    /// <param name="inState">The state in which to test.</param>
    /// <returns>True if a state is a goal state.</returns>
    public bool IsGoal(State inState)
    {
        if (inState.VisitedCheckpoints.Any(c => c))
            Debug.Log("partway there!");

        if (inState.VisitedCheckpoints.Any(s => !s))
        {
            return false;
        }
        Debug.Log("found a goal!");
        return true;
    }

    /// <summary>
    /// Returns the starting state of the vehicle.
    /// </summary>
    /// <returns>Default start state.</returns>
    public State GetInitialState()
    {
        return new State
        {
            CarPosition = transform.position,
            CarVelocity = Vector3.zero,
            CarForward = transform.forward,
            CarUp = transform.up,
            VisitedCheckpoints = new bool[mCheckpoints.Length],
            PathToHere = new List<MoveInfo> { new MoveInfo { GasAmount = 1, TurnAmount = 0} }
        };
    }

    /// <summary>
    /// Action Cost = Reciprical product of how much gas we use and the standard physical time step.
    /// </summary>
    /// <param name="inMoveInfo">The action to be executed by the vehicle.</param>
    /// <returns>Action cost.</returns>
    public float GetCostOfAction(MoveInfo inMoveInfo)
    {
        return (1.0f / Mathf.Abs(inMoveInfo.GasAmount)) * kTimeStepPerMove;
    }

    /// <summary>
    /// Given a state, calculates the cost given by the heuristic.
    /// </summary>
    /// <param name="inState"></param>
    /// <returns></returns>
    public float GetHeuristicCost(State inState)
    {
        float minCheckpointDistance = float.MaxValue;
        int minIndex = -1;
        testCar.transform.forward = inState.CarForward;
        testCar.transform.position = inState.CarPosition;

        int unvisitedCount = 0;
        for (int index = 0; index < mCheckpoints.Length; index++)
        {
            var checkpoint = mCheckpoints[index];
            if (!inState.VisitedCheckpoints[index])
            {
                float thisDist = Vector3.Distance(checkpoint.transform.position, inState.CarPosition);
                if (thisDist < minCheckpointDistance)
                {
                    minCheckpointDistance = thisDist;
                    minIndex = index;
                }
                unvisitedCount++;
            }
        }
        float noZoneDistSum = 0;
        var noZoneDists = new List<float>();
        foreach (var nozone in mNoZones)
        {
            if (!showInconsistency)
            {
                float nzd = Mathf.Sqrt(nozone.GetComponent<Collider>().bounds.SqrDistance(inState.CarPosition));
                noZoneDistSum += nzd;
                noZoneDists.Add(nzd);
            }
            else
            {
                float disttoNoZone = Vector3.Distance(mCheckpoints[minIndex].transform.position, nozone.bounds.center);
                if (disttoNoZone < minCheckpointDistance)
                {
                    float nzd = Mathf.Sqrt(nozone.GetComponent<Collider>().bounds.SqrDistance(inState.CarPosition));
                    noZoneDistSum += nzd;
                    noZoneDists.Add(nzd);
                }
            }
        }

        if (Math.Abs(minCheckpointDistance - float.MaxValue) < 0.1f)
        {
            Debug.Log("returning goal state");
            minCheckpointDistance = 0;
        }
        float noZoneDist = 0;
        if (noZoneDists.Count > 0)
        {
            noZoneDist = noZoneDists.Min();
        }

        switch (heuristicType)
        {
            case HeuristicType.FullHeuristic:
                return ((minCheckpointDistance) - (noZoneDist / 1.5f)); // Actual Heuristic
            case HeuristicType.GoalDistance:
                return minCheckpointDistance;                           // Display For Distance to Goal
            case HeuristicType.ObstacleAvoidance:
                return noZoneDist;                    // Display For Block Avoidance
        }
        return 0;
    }

    /// <summary>
    /// Given a state and the Move info constraints, get all possible states.
    /// </summary>
    /// <param name="inState">The state to generate successors from.</param>
    /// <returns></returns>
    public List<State> GetSuccessors(State inState)
    {
        var newStates = new List<State>();
        // Gas values can go -1 to 1 but implementation is not optimized
        float[] gasValues = { 0.3333f, 0.6666f, 1.0f};
        float[] turnValues = { -1.0f, -0.6666f, -0.3333f, 0f, 0.3333f, 0.6666f, 1.0f };
        foreach (var gas in gasValues)
        {
            foreach (var turn in turnValues)
            {
                float turnDegrees = mTurnVelocity * turn;
                var newForward = Quaternion.AngleAxis(turnDegrees, inState.CarUp) * inState.CarForward;
                var newVelocity = inState.CarVelocity + (newForward * mForwardVelocity * gas);
                newVelocity = Vector3.ClampMagnitude(newVelocity, mForwardVelocity);
                Vector3 newPosition = inState.CarPosition + (newVelocity * Time.fixedDeltaTime);
                bool[] newCheckPoints = inState.VisitedCheckpoints.DeepClone();

                testCar.transform.forward = newForward;
                testCar.transform.position = newPosition;

                for (int checkpointIndex = 0; checkpointIndex < mCheckpoints.Length; checkpointIndex++)
                {
                    var checkpoint = mCheckpoints[checkpointIndex];

                    if (checkpoint.GetComponent<Collider>().bounds.Intersects(testCarCollider.bounds))
                        newCheckPoints[checkpointIndex] = true;
                }
                var newPathToHere = new List<MoveInfo>(inState.PathToHere)
                {
                    new MoveInfo
                    {
                        GasAmount = gas,
                        TurnAmount = turn
                    }
                };

                positionList.Add(newPosition);

                bool any = false;
                foreach (var noZone in mNoZones)
                {
                    if (noZone.bounds.Intersects(testCarCollider.bounds))
                        any = true;
                }
                if (!any)
                {
                    expansionCounter++;
                    newStates.Add(new State
                    {
                        CarVelocity = newVelocity,
                        CarForward = newForward,
                        CarUp = inState.CarUp,
                        CarPosition = newPosition,
                        VisitedCheckpoints = newCheckPoints,
                        PathToHere = newPathToHere
                    });
                }
            }
        }
        return newStates;
    }

    /// <summary>
    /// Helper class to store pairs of information
    /// </summary>
    /// <typeparam name="TFirst">The first pair value.</typeparam>
    /// <typeparam name="TSecond">The second pair value.</typeparam>
    public class Pair<TFirst, TSecond>
    {
        public TFirst First;
        public TSecond Second;

        public Pair(TFirst inFirst, TSecond inSecond)
        {
            First = inFirst;
            Second = inSecond;
        }
    }

    /// <summary>
    /// Our state representation of the vehicle.
    /// CarUp is not expressly used yet but will be needed for air obstacles once physics is added in.
    /// </summary>
    public struct State
    {
        public Vector3 CarPosition;
        public Vector3 CarVelocity;
        public Vector3 CarForward;
        public Vector3 CarUp;
        public bool[] VisitedCheckpoints;
        public List<MoveInfo> PathToHere;
    }

    /// <summary>
    /// An enum for the type of heuristic we want to use.
    /// </summary>
    private enum HeuristicType
    {
        FullHeuristic,
        GoalDistance,
        ObstacleAvoidance
    }

    /// <summary>
    /// Event handler that is fired by Unity. This draws the state representation and Heuristic cost coloring.
    /// </summary>
    private void OnDrawGizmos()
    {
        // Heuristic Display
        if (showHeuristicDisplay && testCar)
        {
            const float minX = -30;
            const float maxX = 10;
            const float minY = -5;
            const float maxY = 95;
            const float increment = 1f;
            if (!calculated)
            {
                dataPoints = new float[(int)((maxX - minX) / increment), (int)((maxY - minY) / increment)];
                var vcp = new bool[3];
                for (var x = minX; x < maxX; x += increment)
                {
                    for (var z = minY; z < maxY; z += increment)
                    {
                        dataPoints[(int)((x - minX) / increment), (int)((z - minY) / increment)] = GetHeuristicCost(new State
                        {
                            CarPosition = new Vector3(x, 0, z),
                            VisitedCheckpoints = vcp,
                            CarForward = Vector3.forward
                        });
                    }
                }
                maxDP = 0;
                minDP = float.MaxValue;
                foreach (var datapoint in dataPoints)
                {
                    if (datapoint != THIS_IS_REALLY_BAD)
                    {
                        maxDP = Mathf.Max(maxDP, datapoint);
                        minDP = Mathf.Min(minDP, datapoint);
                    }
                }

                maxDP -= minDP;
            }
            for (var x = minX; x < maxX; x += increment)
            {
                for (var z = minY; z < maxY; z += increment)
                {
                    float num = dataPoints[(int)((x - minX) / increment), (int)((z - minY) / increment)];
                    float goodnum = (num - minDP) / (maxDP);
                    Gizmos.color = Color.Lerp(Color.green, Color.red, Mathf.Pow(goodnum, 1f));
                    Gizmos.DrawCube(new Vector3(x, 0.05f, z), new Vector3(increment, 0.001f, increment));
                }
            }
        }

        Gizmos.color = Color.yellow;
        foreach (var vector3 in positionList)
        {
            Gizmos.DrawWireCube(vector3, Vector3.one * 0.1f);
        }

        Gizmos.color = Color.blue;
        foreach (var vector3 in actualPositionList)
        {
            Gizmos.DrawWireCube(vector3, Vector3.one * 0.1f);
        }
    }
}

/// <summary>
/// Helper class made in order to deep clone Move Infos and States.
/// </summary>
public static class ExtensionMethods
{
    /// <summary>
    /// Custom cloning function for closed list.
    /// </summary>
    public static T DeepClone<T>(this T a)
    {
        using (var stream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, a);
            stream.Position = 0;
            return (T)formatter.Deserialize(stream);
        }
    }
}
