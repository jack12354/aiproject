﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

/// <summary>
/// A object that a vehicle navigates to.
/// </summary>
public class Checkpoint : MonoBehaviour
{
    /// <summary>
    /// Have we visited this checkpoint?
    /// </summary>
    public bool IsVisited;
    /// <summary>
    /// Is the checkpoint the final goal point?
    /// </summary>
    public bool IsGoal = false;
    /// <summary>
    /// True if all checkpoints including the goal are visited.
    /// </summary>
    private bool IsComplete = true;
    private List<Checkpoint> mCheckpoints = new List<Checkpoint>();
    private Collider mCarCollider;

    /// <summary>
    /// Start methods in Unity are run the first frame of the Unity engine. Used to cache objects.
    /// </summary>
    void Start()
    {
        mCarCollider = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>();
        GetComponent<Renderer>().material.color = Color.red;
        if (IsGoal)
        {
            GetComponent<Renderer>().material.color = new Color(0.5f, 0,0);

            IsComplete = false;
            mCheckpoints.AddRange(FindObjectsOfType<Checkpoint>());
            mCheckpoints.Remove(this);
            Assert.IsFalse((mCheckpoints.Any(cp => cp.IsGoal)), "Only one goal allowed!");
        }
    }

    /// <summary>
    /// Unity calls this method once every frame. This method is used for consistent checks during the running time of the Unity engine. 
    /// </summary>
    void Update()
    {
        // Have we reached the goal in Unity runtime?
        if (mCarCollider.bounds.Intersects(GetComponent<Collider>().bounds))
        {
            IsVisited = true;
            GetComponent<Renderer>().material.color = Color.green;
        }
        if (IsGoal && mCheckpoints.All(cp => cp.IsVisited))
        {
            IsComplete = true;
        }
    }
}
