﻿using System;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The controller behind a vehicle. Currently there are only two subclasses, human and AI.
/// But having this class abstract allows for many different types of AI brains.
/// </summary>
public abstract class Brain : MonoBehaviour
{
    /// <summary>
    /// Types of agents.
    /// </summary>
    public enum BrainType { Human, AI }
    /// <summary>
    /// Maps brain type to object type.
    /// </summary>
    public static Dictionary<BrainType, Type> BrainTypeToTypes = new Dictionary<BrainType, Type>();
    /// <summary>
    /// Flag used to tell the agent to start.
    /// </summary>
    protected bool mEnabled = false;
    /// <summary>
    /// Agent's goal objects to navigate to.
    /// </summary>
    protected Checkpoint[] mCheckpoints;
    /// <summary>
    /// Agent's obstacles to avoid while navigating to goal.
    /// </summary>
    protected Collider[] mNoZones;
    /// <summary>
    /// The speed the car is allowed to move, set in editor.
    /// </summary>
    protected float mForwardVelocity;
    /// <summary>
    /// The speed the car is allowed to turn, set in editor.
    /// </summary>
    protected float mTurnVelocity;


    /// <summary>
    /// Static constructor that fetches the type of brain using a mapping dictionary.
    /// </summary>
    static Brain()
    {
        BrainTypeToTypes[BrainType.Human] = typeof(HumanBrain);
        BrainTypeToTypes[BrainType.AI] = typeof(AIBrain);
    }


    /// <summary>
    /// This is similar to a constructor but for the Unity Running environment. Needed to be called seperately because game objects exist in the unity space.
    /// </summary>
    /// <param name="inCheckpoints">An array of checkpoint objects in the scene. Mainly used to get position of objects.</param>
    /// <param name="inNoZones">An array of obstacle objects in the scene. Mainly used to get the position and dimentions of the objects.</param>
    /// <param name="inGasAmount">How fast the brain allows the vehicle to go.</param>
    /// <param name="inTurnAmount">Maximum degree a brain allows the vehicle to turn.</param>
    public abstract void Init(Checkpoint[] inCheckpoints, Collider[] inNoZones, float inGasAmount, float inTurnAmount);


    /// <summary>
    /// Called once per frame from vehicle to gets the current vehicle control state representation.
    /// </summary>
    /// <returns>A move info object containing turn and gas amounts.</returns>
    public abstract MoveInfo GetMoveInfo();


    /// <summary>
    /// Runs the process of goal finding.
    /// </summary>
    public abstract void BuildPathToGoal();


    /// <summary>
    /// Called when pressing the g key in Unity. Tells the program to start building the path to the goal.
    /// </summary>
    public abstract void Go();
}


/// <summary>
/// A representation of how the agent will move in the world.
/// </summary>
public struct MoveInfo : ICloneable
{
    /// <summary>
    /// -1.0f &lt;= x &lt;= 1.0f | Full Left Turn &lt;= x &lt;= Full Right Turn
    /// </summary>
    public float TurnAmount;
    /// <summary>
    ///  -1.0f &lt;= x &lt;= 1.0f | Full Gas &lt;= x &lt;= Full Brake
    /// </summary>
    public float GasAmount;


    public object Clone()
    {
        return MemberwiseClone();
    }
}
