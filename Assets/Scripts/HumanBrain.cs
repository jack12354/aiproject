﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// A human agent to find the goal checkpoint.
/// </summary>
public class HumanBrain : Brain
{
    /// <summary>
    /// Gets back a move info object for the human => car interaction
    /// </summary>
    /// <returns>MoveInfo object based on the human input Vertical and Hortizontal Axis or an empty Move Info object</returns>
    public override MoveInfo GetMoveInfo()
    {
        if (mEnabled)
        {
            return new MoveInfo
            {
                GasAmount = CrossPlatformInputManager.GetAxis("Vertical"),
                TurnAmount = CrossPlatformInputManager.GetAxis("Horizontal")
            };
        }
        return new MoveInfo();
    }

    // These functions are not needed for human control
    public override void BuildPathToGoal() { }
    public override void Go() { }
    public override void Init(Checkpoint[] mCheckpoints, Collider[] inNoZones, float inGasAmount, float inTurnAmount) { }
}
