﻿using System;
using UnityEngine;

/// <summary>
/// Class responsible for taking move infos and translating that to Unity's engine.
/// </summary>
public class Vehicle : MonoBehaviour
{
    /// <summary>
    /// Set this in editor.
    /// </summary>
    public Brain.BrainType brainType = Brain.BrainType.Human;
    /// <summary>
    /// The Maximum speed of the car. Set in editor.
    /// </summary>
    public float MaxSpeed;
    /// <summary>
    /// The maximum turning speed of the car. Set in editor.
    /// </summary>
    public float MaxTurnSpeed;
    /// <summary>
    /// The vehicle's brain or operator.
    /// </summary>
    private Brain mBrain;
    /// <summary>
    /// The real time velocity of the vehicle.
    /// </summary>
    private Vector3 Velocity = Vector3.zero;

    /// <summary>
    /// Unity calls this function once on runtime.
    /// </summary>
    void Start()
    {
        var checkpoints = FindObjectsOfType<Checkpoint>();
        var noZonesGOs = GameObject.FindGameObjectsWithTag("NO");
        var noZones = new Collider[noZonesGOs.Length];
        for (int index = 0; index < noZonesGOs.Length; index++)
        {
            noZones[index] = noZonesGOs[index].GetComponent<Collider>();
        }

        mBrain = gameObject.AddComponent(Brain.BrainTypeToTypes[brainType]) as Brain;
        if (mBrain == null) return;
        mBrain.Init(checkpoints, noZones, MaxSpeed, MaxTurnSpeed);
        Debug.Log("Ready to go.");
    }

    /// <summary>
    /// Unity calls this method every frame. Checks for key press.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            mBrain.Go();
        }

    }

    /// <summary>
    /// Unity calls this every physics frame.
    /// </summary>
    private void FixedUpdate()
    {
        MoveInfo thisMove = mBrain.GetMoveInfo();
        // If we get signal that we reached goal, stop.
        if (Math.Abs(thisMove.GasAmount - 100.0f) < 0.001f) return;

        float turnDegrees = MaxTurnSpeed * thisMove.TurnAmount;
        Vector3 newForward = Quaternion.AngleAxis(turnDegrees, transform.up) * transform.forward;
        Vector3 newVelocity = Velocity + (newForward * MaxSpeed * thisMove.GasAmount);

        // Do not go passed our max speed
        Velocity = Vector3.ClampMagnitude(newVelocity, MaxSpeed);

        // Move car position and forward when given move info.
        transform.position = transform.position + (Velocity * Time.fixedDeltaTime);
        transform.forward = newForward;
    }
}


